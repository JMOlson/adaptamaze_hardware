Analog_SplitterAdaptAMaze: Splits analog input signals to light LEDs and (depreciated) electric motor control

Connector_PCB_AdaptAMaze: PCB that goes into well stack connector. Receives 3.5mm (lick sensors) and 2.5mm (light LEDs) audio cables from command boards and combines into 1 Molex connector for Well PCB.

ECU DI_DO_40Pin_Splitter_AdaptAMaze: Splits the ECU ribbon cable (that carries both inputs and outsputs) into separate inputs and outputs resulting in 2 dedicated ribbon cables (one with only inputs and one with only outputs).

Reward_Pump_Command_AdaptAMaze: Distributes output ribbon cable from ECU to individual reward pumps. Also handles power distribution to pumps.

Well_Detector_Command_AdaptAMaze: Distributes input ribbon cable from ECU to each reward well for lick detection. Also handles power distribution to IR emitters and phototransducers.

Well_LED_Command_AdaptAMaze: Distributes output ribbon cable from ECU to each reward well for light LEDs. Handles power to light LEDs. However, power is fed from the Arduino attached to this PCB rather than directly powered like the lick IR emitter/sensors.

Well_PCB_AdaptAMaze: PCB that is attached to a reward well. Molex connector goes to leg Connector PCB. Well PCB contains IR LEDs for lick sensors and light LEDs to light up reward wells. 

