<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="FRAME_A_L" urn="urn:adsk.eagle:symbol:13882/1" library_version="1">
<frame x1="0" y1="0" x2="279.4" y2="215.9" columns="6" rows="5" layer="94" border-bottom="no"/>
</symbol>
<symbol name="DOCFIELD" urn="urn:adsk.eagle:symbol:13864/1" library_version="1">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94">&gt;DRAWING_NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="FRAME_A_L" urn="urn:adsk.eagle:component:13939/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt; A Size , 8 1/2 x 11 INCH, Landscape&lt;p&gt;</description>
<gates>
<gate name="G$1" symbol="FRAME_A_L" x="0" y="0" addlevel="always"/>
<gate name="G$2" symbol="DOCFIELD" x="172.72" y="0" addlevel="always"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SnapEDA-Library">
<packages>
<package name="TE_5-103167-5">
<wire x1="-4.826" y1="-12.738" x2="-4.826" y2="3.493" width="0.127" layer="51"/>
<wire x1="-4.826" y1="3.493" x2="22.606" y2="3.493" width="0.127" layer="51"/>
<wire x1="22.606" y1="3.493" x2="22.606" y2="-12.738" width="0.127" layer="51"/>
<wire x1="22.606" y1="-12.738" x2="-4.826" y2="-12.738" width="0.127" layer="51"/>
<wire x1="-4.826" y1="-12.738" x2="-4.826" y2="-12.738" width="0.127" layer="51"/>
<wire x1="-4.826" y1="-12.738" x2="22.606" y2="-12.738" width="0.127" layer="51"/>
<wire x1="22.606" y1="-12.738" x2="22.606" y2="-12.738" width="0.127" layer="51"/>
<wire x1="-4.826" y1="-12.738" x2="-4.826" y2="3.493" width="0.127" layer="21"/>
<wire x1="22.606" y1="3.493" x2="22.606" y2="-12.738" width="0.127" layer="21"/>
<wire x1="22.606" y1="3.493" x2="-4.826" y2="3.493" width="0.127" layer="21"/>
<wire x1="22.606" y1="-12.738" x2="-4.826" y2="-12.738" width="0.127" layer="21"/>
<wire x1="-5.076" y1="-12.988" x2="-5.076" y2="3.743" width="0.05" layer="39"/>
<wire x1="-5.076" y1="3.743" x2="22.856" y2="3.743" width="0.05" layer="39"/>
<wire x1="22.856" y1="3.743" x2="22.856" y2="-12.988" width="0.05" layer="39"/>
<wire x1="22.856" y1="-12.988" x2="-5.076" y2="-12.988" width="0.05" layer="39"/>
<text x="-5.826" y="4.742" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.826" y="-14.988" size="1.27" layer="27">&gt;VALUE</text>
<circle x="-5.476" y="0" radius="0.1" width="0.2" layer="21"/>
<circle x="-5.476" y="0" radius="0.1" width="0.2" layer="51"/>
<pad name="1" x="0" y="0" drill="0.89" diameter="1.24" shape="square"/>
<pad name="2" x="0" y="2.54" drill="0.89" diameter="1.24"/>
<pad name="3" x="2.54" y="0" drill="0.89" diameter="1.24"/>
<pad name="4" x="2.54" y="2.54" drill="0.89" diameter="1.24"/>
<pad name="5" x="5.08" y="0" drill="0.89" diameter="1.24"/>
<pad name="6" x="5.08" y="2.54" drill="0.89" diameter="1.24"/>
<pad name="7" x="7.62" y="0" drill="0.89" diameter="1.24"/>
<pad name="8" x="7.62" y="2.54" drill="0.89" diameter="1.24"/>
<pad name="9" x="10.16" y="0" drill="0.89" diameter="1.24"/>
<pad name="10" x="10.16" y="2.54" drill="0.89" diameter="1.24"/>
<pad name="11" x="12.7" y="0" drill="0.89" diameter="1.24"/>
<pad name="12" x="12.7" y="2.54" drill="0.89" diameter="1.24"/>
<pad name="13" x="15.24" y="0" drill="0.89" diameter="1.24"/>
<pad name="14" x="15.24" y="2.54" drill="0.89" diameter="1.24"/>
<pad name="15" x="17.78" y="0" drill="0.89" diameter="1.24"/>
<pad name="16" x="17.78" y="2.54" drill="0.89" diameter="1.24"/>
</package>
<package name="JST_S3B-PH-K-S(LF)(SN)">
<wire x1="5.95" y1="1.35" x2="-1.95" y2="1.35" width="0.127" layer="51"/>
<wire x1="-1.95" y1="1.35" x2="-1.95" y2="-6.25" width="0.127" layer="51"/>
<wire x1="-1.95" y1="-6.25" x2="5.95" y2="-6.25" width="0.127" layer="51"/>
<wire x1="5.95" y1="-6.25" x2="5.95" y2="1.35" width="0.127" layer="51"/>
<circle x="-0.6" y="1.9" radius="0.1" width="0.2" layer="21"/>
<wire x1="6.2" y1="-6.5" x2="-2.2" y2="-6.5" width="0.05" layer="39"/>
<wire x1="-2.2" y1="-6.5" x2="-2.2" y2="1.6" width="0.05" layer="39"/>
<wire x1="-2.2" y1="1.6" x2="6.2" y2="1.6" width="0.05" layer="39"/>
<wire x1="6.2" y1="1.6" x2="6.2" y2="-6.5" width="0.05" layer="39"/>
<text x="-1.70185" y="2.55378125" size="1.27043125" layer="25">&gt;NAME</text>
<text x="-1.905009375" y="-7.83341875" size="1.271159375" layer="27">&gt;VALUE</text>
<wire x1="5.95" y1="1.35" x2="-1.95" y2="1.35" width="0.127" layer="21"/>
<wire x1="-1.95" y1="1.35" x2="-1.95" y2="-6.25" width="0.127" layer="21"/>
<wire x1="-1.95" y1="-6.25" x2="5.95" y2="-6.25" width="0.127" layer="21"/>
<wire x1="5.95" y1="-6.25" x2="5.95" y2="1.35" width="0.127" layer="21"/>
<circle x="-0.6" y="1.9" radius="0.1" width="0.2" layer="51"/>
<pad name="1" x="0" y="0" drill="0.8" shape="square" rot="R180"/>
<pad name="2" x="2" y="0" drill="0.8" rot="R180"/>
<pad name="3" x="4" y="0" drill="0.8" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="5-103167-5">
<wire x1="-5.08" y1="-20.32" x2="-5.08" y2="22.86" width="0.254" layer="94"/>
<wire x1="-5.08" y1="22.86" x2="5.08" y2="22.86" width="0.254" layer="94"/>
<wire x1="5.08" y1="22.86" x2="5.08" y2="-20.32" width="0.254" layer="94"/>
<wire x1="5.08" y1="-20.32" x2="-5.08" y2="-20.32" width="0.254" layer="94"/>
<text x="-5.58" y="22.86" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-22.86" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-10.16" y="20.32" length="middle" direction="pas"/>
<pin name="2" x="-10.16" y="17.78" length="middle" direction="pas"/>
<pin name="3" x="-10.16" y="15.24" length="middle" direction="pas"/>
<pin name="4" x="-10.16" y="12.7" length="middle" direction="pas"/>
<pin name="5" x="-10.16" y="10.16" length="middle" direction="pas"/>
<pin name="6" x="-10.16" y="7.62" length="middle" direction="pas"/>
<pin name="7" x="-10.16" y="5.08" length="middle" direction="pas"/>
<pin name="8" x="-10.16" y="2.54" length="middle" direction="pas"/>
<pin name="9" x="-10.16" y="0" length="middle" direction="pas"/>
<pin name="10" x="-10.16" y="-2.54" length="middle" direction="pas"/>
<pin name="11" x="-10.16" y="-5.08" length="middle" direction="pas"/>
<pin name="12" x="-10.16" y="-7.62" length="middle" direction="pas"/>
<pin name="13" x="-10.16" y="-10.16" length="middle" direction="pas"/>
<pin name="14" x="-10.16" y="-12.7" length="middle" direction="pas"/>
<pin name="15" x="-10.16" y="-15.24" length="middle" direction="pas"/>
<pin name="16" x="-10.16" y="-17.78" length="middle" direction="pas"/>
</symbol>
<symbol name="S3B-PH-K-S(LF)(SN)">
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<text x="-5.082559375" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.080559375" y="-7.621390625" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-10.16" y="2.54" length="middle" direction="pas"/>
<pin name="2" x="-10.16" y="0" length="middle" direction="pas"/>
<pin name="3" x="-10.16" y="-2.54" length="middle" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="5-103167-5" prefix="J">
<gates>
<gate name="G$1" symbol="5-103167-5" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TE_5-103167-5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER" value="TE"/>
<attribute name="PARTREV" value="R"/>
<attribute name="STANDARD" value="Manufacturer Recommendations"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="S3B-PH-K-S(LF)(SN)" prefix="J">
<description>Connector Header Through Hole, Right Angle 3 position 0.079" (2.00mm) </description>
<gates>
<gate name="G$1" symbol="S3B-PH-K-S(LF)(SN)" x="0" y="0"/>
</gates>
<devices>
<device name="" package="JST_S3B-PH-K-S(LF)(SN)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER" value="JST Sales"/>
<attribute name="MAXIMUM_PACKAGE_HEIGHT" value="4.8mm"/>
<attribute name="PARTREV" value=""/>
<attribute name="STANDARD" value="Manufacturer Recommendations"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="FRAME_A_L" device=""/>
<part name="ECU_AIO" library="SnapEDA-Library" deviceset="5-103167-5" device=""/>
<part name="MOTOR_COMMAND" library="SnapEDA-Library" deviceset="S3B-PH-K-S(LF)(SN)" device=""/>
<part name="LED_COMMAND" library="SnapEDA-Library" deviceset="S3B-PH-K-S(LF)(SN)" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="FRAME1" gate="G$2" x="172.72" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="185.42" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="259.08" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="190.5" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="ECU_AIO" gate="G$1" x="261.62" y="81.28" smashed="yes">
<attribute name="NAME" x="256.04" y="106.68" size="1.778" layer="95"/>
<attribute name="VALUE" x="256.54" y="58.42" size="1.778" layer="96"/>
</instance>
<instance part="MOTOR_COMMAND" gate="G$1" x="208.28" y="45.72" smashed="yes" rot="R180">
<attribute name="NAME" x="213.362559375" y="39.878" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="213.360559375" y="53.341390625" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="LED_COMMAND" gate="G$1" x="220.98" y="71.12" smashed="yes" rot="R180">
<attribute name="NAME" x="226.062559375" y="65.278" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="226.060559375" y="78.741390625" size="1.778" layer="96" rot="R180"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="ECU_AIO" gate="G$1" pin="11"/>
<wire x1="251.46" y1="76.2" x2="231.14" y2="76.2" width="0.1524" layer="91"/>
<label x="243.84" y="76.2" size="1.778" layer="95" rot="MR0"/>
<wire x1="231.14" y1="76.2" x2="231.14" y2="73.66" width="0.1524" layer="91"/>
<pinref part="LED_COMMAND" gate="G$1" pin="3"/>
</segment>
<segment>
<pinref part="ECU_AIO" gate="G$1" pin="12"/>
<wire x1="251.46" y1="73.66" x2="243.84" y2="73.66" width="0.1524" layer="91"/>
<wire x1="243.84" y1="73.66" x2="243.84" y2="48.26" width="0.1524" layer="91"/>
<label x="238.76" y="48.26" size="1.778" layer="95" rot="MR0"/>
<pinref part="MOTOR_COMMAND" gate="G$1" pin="3"/>
<wire x1="218.44" y1="48.26" x2="243.84" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BARRIER_RESET" class="0">
<segment>
<pinref part="ECU_AIO" gate="G$1" pin="15"/>
<wire x1="251.46" y1="66.04" x2="246.38" y2="66.04" width="0.1524" layer="91"/>
<wire x1="246.38" y1="66.04" x2="246.38" y2="45.72" width="0.1524" layer="91"/>
<pinref part="MOTOR_COMMAND" gate="G$1" pin="2"/>
<wire x1="246.38" y1="45.72" x2="218.44" y2="45.72" width="0.1524" layer="91"/>
<label x="243.84" y="45.72" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="BARRIER_SIG" class="0">
<segment>
<pinref part="ECU_AIO" gate="G$1" pin="16"/>
<wire x1="251.46" y1="63.5" x2="248.92" y2="63.5" width="0.1524" layer="91"/>
<wire x1="248.92" y1="63.5" x2="248.92" y2="43.18" width="0.1524" layer="91"/>
<label x="248.92" y="43.18" size="1.778" layer="95" rot="MR0"/>
<pinref part="MOTOR_COMMAND" gate="G$1" pin="1"/>
<wire x1="248.92" y1="43.18" x2="218.44" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED_SIG2" class="0">
<segment>
<pinref part="ECU_AIO" gate="G$1" pin="13"/>
<pinref part="LED_COMMAND" gate="G$1" pin="2"/>
<wire x1="251.46" y1="71.12" x2="231.14" y2="71.12" width="0.1524" layer="91"/>
<label x="243.84" y="71.12" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="LED_SIG1" class="0">
<segment>
<pinref part="ECU_AIO" gate="G$1" pin="14"/>
<pinref part="LED_COMMAND" gate="G$1" pin="1"/>
<wire x1="251.46" y1="68.58" x2="231.14" y2="68.58" width="0.1524" layer="91"/>
<label x="243.84" y="68.58" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
