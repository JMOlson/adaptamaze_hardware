# AdaptAMaze PCBs

We have made a few PCBs for the AdaptAMaze system to make connectivity for signals and power easier. For most PCBs, we have added interfaces for Arduinos. This could allow Arduinos to control things like rewards or detect licks, or help with debugging. However, Arduinos are not necessary and you can hook up the relevant PCBs directly to your SpikeGadgets ECU or other controller. 

The Parts List contains all additional components and wires for the PCBs.

## Well & Leg Stack Nomenclature
- **Analog_SplitterAdaptAMaze**: Splits analog input signals to light LEDs and (depreciated) electric motor control.

- **Connector_PCB_AdaptAMaze**: PCB that goes into Well stack's 3D printed Connector. Receives 3.5mm (lick sensors) and 2.5mm (light LEDs) audio cables from command boards and combines into 1 Molex connector for Reward Well PCB.

- **ECU DI_DO_40Pin_Splitter_AdaptAMaze**: Only needed for SpikeGadgets ECU. Splits the SpikeGadget's ECU ribbon cable (that carries both inputs and outputs) into separate inputs and outputs resulting in 2 dedicated ribbon cables (one with only inputs and one with only outputs).

- **Reward_Pump_Command_AdaptAMaze**: Distributes ECU's output ribbon cable from ECU to individual reward pumps. Also handles 12V power distribution to pumps via barrel jacks.

- **Well_Detector_Command_AdaptAMaze**: Distributes ECU's input ribbon cable to each Reward well's Connector for lick detection. Also handles power distribution to IR emitters and phototransducers.

- **Well_LED_Command_AdaptAMaze**: Distributes ECU's output ribbon cable from ECU to each reward well for light LEDs. Handles power to light LEDs. However, power is fed from the Arduino attached to this PCB rather than directly powered like the lick IR emitter/sensors.

- **Well_PCB_AdaptAMaze**: PCB that is attached to a Reward well. Molex connector goes to  Connector PCB. Well PCB contains IR LEDs for lick sensors and light LEDs to light up reward wells.

## Provided Files
- EAGLE
- kiCAD
- Gerber