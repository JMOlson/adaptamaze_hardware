# AdaptAMaze Well & Leg Stack

The Well & Leg stack refers to all the parts that support that track pieces as well as function as reward wells where needed. If reward wells are not needed at a location, "plugs" can be used instead of reward wells. 

The legs themselves are T slot aluminum. All other components are 3D printed and files are provided in this folder. 

See Parts List for all necessary parts including PCBs, circuit components, and wiring.

## Well & Leg Stack Nomenclature
- **BasePlate:** Attaches to the top of the leg with screws to interface with the Connector.
    - We recommend printing with a filament printer in PLA.
- **Connector:** Intermediary between BasePlate and Reward well or Plug. For a reward well, also contains the Connector PCB which receives the 3.5mm and 2.5mm audio cables for IR beam break and visible light, respectively, and connects to the reward well PCB via Molex. Reward tubing passes through.
    - We recommend printing with a filament printer in PLA.
- **Reward Well:** Slots through a track piece and screws into the Connector. Hole in center for reward delivery. Two pairs of IR LEDs possible for lick detection via beam break. Two visible light LEDs possible for cueing. Holds the reward well PCB. 
    - We recommend printing with an SLA printer for smoother screw threads and a smoother finish than most filament printers. We recommend clear resin for the light LEDs to illuminate the reward well if needed.
- **Plug:** Slots through a track piece and screws into the Connector. Used on track pieces that do not need reward wells. 
    - We recommend printing with the same material as your reward wells for consistency.


## Provided Files

- STL
- gcode
- STEP
- SLDPRT & SLDASM



