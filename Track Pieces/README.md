# AdaptAMaze Track Pieces

Aluminum sheet metal track pieces for rodents to run on.

All parts were created in Solidworks. All parts are in the Track Pieces folder. Associated files, including schematic drawings, can be found in the relevant subfolder.

Please see the parts list for detailed ordering instructions.

## Track Pieces Nomenclature

- **baseTrackPiece**: Base model for making standard 18" track pieces. This piece could be used in conjunction with "circular pattern" to create almost every track piece.
- **Corner_branch**: A 90° bend with a small track coming off the apex. The two other tracks are longer.
- **Corner_branch_long**: A 90° bend with a long track coming off the apex. As a result, the two other tracks are shorter.
- **I**: Standard straight track piece.
- **I_Plat**: Straight track piece with a small platform in the center.
- **L**: Standard 90° bend.
- **L_long**: 90° bend with both arms longer than standard L.
- **Oct**: Center for 8 arm radial arm maze.
- **Plat**: Large terminating platform.
- **Plat_small**: Small terminating platform.
- **Plus**: Standard 4 arm junction. Used for plus maze or making node on a grid.
- **T**: Standard T junction.
- **T_long**: T junction with all arms equally lengthened.
- **Y**: Standard Y track. 3 arms at 120°.

## Provided Files

- SLDPRT
- STEP
- DXF 
- eDrawings
- SLDDRAW
- PDF of SLDDRW
- Pictures for quick reference




